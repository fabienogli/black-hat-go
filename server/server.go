package server

import (
	"log"
	"net"
)

// Server Interface abstract handling of connection
type Server interface {
	Handle(conn net.Conn)
}

// RunAndListen serve a tcp server on the address given and handle connection with the inter method
func RunAndListen(address string, inter Server) {
	//Listen on local port 80
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalln("Unable to bind to port")
	}
	log.Printf("Listening on %s", listener.Addr())

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalln("Unable to accept connection")
		}
		go inter.Handle(conn)
	}
}
