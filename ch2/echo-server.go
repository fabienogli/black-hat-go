package ch2

import (
	"io"
	"log"
	"net"

	"gitlab.com/fabienogli/black-hat-go/server"
)

type echoServer struct{}

//echo is a handler function that simply echoes received data
func (e echoServer) Handle(conn net.Conn) {
	defer conn.Close()

	if _, err := io.Copy(conn, conn); err != nil {
		log.Fatalln("Unable to read/write data")
	}
}

func EchoServer() {
	e := echoServer{}
	server.RunAndListen(":20080", e)
}
