package ch2

import (
	"io"
	"log"
	"net"

	"gitlab.com/fabienogli/black-hat-go/server"
)

type proxy struct {
	Address string
}

func (p proxy) Handle(src net.Conn) {
	dst, err := net.Dial("tcp", p.Address)
	if err != nil {
		log.Fatalln("Unable to connect to our unreachable host")
	}
	defer dst.Close()

	//Run in goroutine to prevent io.Copy from blocking
	go func() {
		//Copy our source's output to the destination
		if _, err := io.Copy(dst, src); err != nil {
			log.Fatalln(err)
		}
	}()
	// Copy our destination's output back to our source
	if _, err := io.Copy(src, dst); err != nil {
		log.Fatalln(err)
	}
}

func Proxy() {
	p := proxy{
		Address: "www.onisep.fr:80",
	}
	server.RunAndListen(":8080", p)
}
